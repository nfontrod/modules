#!/bin/bash
# Copyright claire burny for the LBMC UMR 5239 ©.
# contributor(s) : claire burny (2017)
#
# claire.burny@ens-lyon.fr
#
# This software is a computer program whose purpose is to manage dated file
# names in complience with the bioinformatic good practices used in the LBMC.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

source ../build_env.sh

CDHIT_VERSION=4.6.8
mkdir -p ${CDHIT_VERSION}/bin

curl -k -L https://github.com/weizhongli/cdhit/releases/download/V${CDHIT_VERSION}/cd-hit-v${CDHIT_VERSION}-2017-0621-source.tar.gz -o cd-hit-v${CDHIT_VERSION}.tar.gz

mkdir cd-hit-v${CDHIT_VERSION} && tar xzf cd-hit-v${CDHIT_VERSION}.tar.gz -C cd-hit-v${CDHIT_VERSION} --strip-components 1

cd cd-hit-v${CDHIT_VERSION}
make
cd cd-hit-auxtools
make
cd ..
cd ..

rsync -av --exclude 'doc/' --exclude 'usecases/' --exclude 'README*' cd-hit-v${CDHIT_VERSION}/ ${CDHIT_VERSION}/bin/

rm -Rf cd-hit-v${CDHIT_VERSION}*
